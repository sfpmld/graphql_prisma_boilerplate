import jwt from 'jsonwebtoken'

const generateToken = (id) => {
  return jwt.sign(
    {
      userId: id
    },
    process.env.JWT_TOKEN,
    { expiresIn: '6h' }
  )
}

const verifyToken = (token) => {
  let decodedToken;

  // verifying token
  try {
    decodedToken = jwt.verify(token, process.env.JWT_TOKEN)
  } catch (err) {
    err.statusCode = 500;
    throw err;
  }

  // if not verify -> user not authorized
  if(!decodedToken){
    const error = new Error('Not authorized.');
    error.statusCode = 401;
    throw error;
  }

  return decodedToken;
}

export { generateToken, verifyToken }
