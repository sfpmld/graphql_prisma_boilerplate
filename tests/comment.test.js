import '@babel/polyfill';
import 'cross-fetch/polyfill';
import { gql } from 'apollo-boost';

import seedDatabase, { userOne, userTwo, postOne, postTwo, commentOne, commentTwo } from './utils/seedDatabase';
import { deleteComment, subscribeToComments } from './utils/operations';

import prisma from '../src/prisma';
import getClient from './utils/getClient'
import getSubClient from './utils/getSubscClient';

// Initialize apollo client
let client = getClient();

// wipe and fetch data before each test case
beforeEach(seedDatabase);

test('should delete own comment', async () => {
  const client = getClient(userTwo.jwt)

  const variables = {
    id: commentOne.comment.id
  }

  const response = await client.mutate({
    mutation: deleteComment,
    variables
  });

  const exists = await prisma.exists.Comment({
      id: commentOne.comment.id
  });

  expect(exists).toBe(false);
});

// jest bug for subscribe test case
// https://www.udemy.com/graphql-bootcamp/learn/lecture/11917840#questions/5989800
test('should not delete other user Comments', async () => {
  const client = getClient(userOne.jwt);

  const variables = {
    id: commentOne.comment.id
  }

  await expect(client.mutate({
    mutation: deleteComment,
    variables
  })).rejects.toThrow();

});

client = getClient(userTwo.jwt);
test('should subscribe to comments for a post', (done) => {

  const wsClient = getSubClient(userOne.jwt);

  const variables = {
    postId: postOne.post.id,
  }

  wsClient.subscribe({
    query: subscribeToComments,
    variables
  }).subscribe({
    next(response){
      expect(response.data.comment.mutation).toBe('DELETED')
      done()
    }
  });

  prisma.mutation.deleteComment({
    where:{
      id: commentTwo.comment.id
    }
  }).then(res => {});
});


