import '@babel/polyfill';
import 'cross-fetch/polyfill';
import { ApolloClient } from "apollo-client";
import { split } from "apollo-link";
import { HttpLink } from "apollo-link-http";
import { WebSocketLink } from "apollo-link-ws";
import { getMainDefinition } from "apollo-utilities";
import { InMemoryCache, NormalizedCacheObject } from "apollo-cache-inmemory";
import { setContext } from "apollo-link-context";
import { onError } from "apollo-link-error";
import gql from "graphql-tag";
import * as fetch from "node-fetch";

// retrieving env vars
// http(s)://...
const GRAPHQL_ENDPOINT = process.env.GRAPHQL_ENDPOINT;
// wss://...

const getSubClient = (jwt, httpURL=GRAPHQL_ENDPOINT) => {

    let errorLink = onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors) {
        graphQLErrors.map(({ message, locations, path }) =>
          console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
          )
        );
      }

      if (networkError) {
        console.log(`[Network error]: ${networkError}`);
      }
    });
    const httpLink = new HttpLink({ uri: httpURL, fetch });
    // credentials: "include"

    const wsLink = new WebSocketLink({
      uri: httpURL.replace("http://", "ws://"),
      options: {
        // reconnect: true,
        inactivityTimeout: 100,
        timeout: 1000,
        connectionParams: () => ({
          token: jwt
        })
      }
    });

    const authLink = setContext((_, { headers }) => {
      // get the authentication token from local storage if it exists
      // return the headers to the context so httpLink can read them
      return { headers: { ...headers, authorization: `Bearer ${jwt}` } };
    });

    return new ApolloClient({
      link: split(
        // split based on operation type
        ({ query }) => {
          const {
            kind,
            operation
          } = getMainDefinition(query);
          return kind === "OperationDefinition" && operation === "subscription";
        },
        wsLink,
        authLink.concat(errorLink).concat(httpLink)
      ),
      cache: new InMemoryCache({ addTypename: false })
    });

}

export { getSubClient as default }
