// to start the server
require('@babel/polyfill/noConflict');
require('regenerator-runtime');
require('@babel/core');

// server
const server = require('../../src/server').default;

module.exports = async () => {
  global.httpServer = await server.start({port: 4000})
}
