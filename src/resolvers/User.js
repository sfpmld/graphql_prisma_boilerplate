import getUserId from '../services/get-userid';

const User = {
  posts: {

    fragment: 'fragment userId on User { id }',
    resolve(parent, args, { prisma, req  }, info){

      // retrieving user ID
      const userId = getUserId(req)

      // retrieving only posts published by the user logged in
      return prisma.query.posts({
        where: {
          data: {
            published: true,
            author: {
              id: parent.id
            }
          }
        }
      })
    }
  },
  email: {
    fragment: 'fragment userId on User { id }',
    resolve(parent, args, { req }, info){

      // retrieving user ID
      const userId = getUserId(req, false)

      if(userId && userId === parent.id){
        return parent.email
      }
      return null
    }
  }

}

export { User as default }

