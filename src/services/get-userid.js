import jwt from 'jsonwebtoken';

//env vars
const JWT_TOKEN = process.env.JWT_TOKEN;

// function to retrieve userId eventually send via token from client
const getUserId = (req, requireAuth=true) => {

    // extracting token from request headers or socker connection when
    // subscription
    const authHeader = req.request ? req.request.headers.authorization : req.connection.context.Authorization;

    if(authHeader){
        const token = authHeader.replace('Bearer ','');

        // verifying token
        let decodedToken;

        try {
          decodedToken = jwt.verify(token, JWT_TOKEN)
        } catch (e) {
          e.status = 401;
          throw e;
        }

        const userId = decodedToken.userId;

        return userId;
    }

    if(requireAuth){
        throw new Error('Authorization is required...');
    }

    return null;

}

export { getUserId as default }
