import { hashPassSync } from '../../src/utils/bcrypt-utils';
import { generateToken } from '../../src/utils/jwt-utils';
import prisma from '../../src/prisma';

const userOne = {
  input: {
    name: "First Tester",
    email: "tester@tes.com",
    password: hashPassSync("password1")
  },
  user: {},
  jwt: undefined
}

const userTwo = {
  input: {
    name: "2d Tester",
    email: "tester2@tes.com",
    password: hashPassSync("password2")
  },
  user: {},
  jwt: undefined
}

const postOne = {
  input: {
    title: "Brand New Post",
    body: "Content Body for testing",
    published: true
  },
  post: {}
}

const postTwo = {
  input: {
    title: "Brand New Post",
    body: "Content Body for testing",
    published: false
  },
  post: {}
}

const commentOne = {
  input: {
    text: "Comment One to test on The published post",
  },
  comment: {}
}

const commentTwo = {
  input: {
    text: "Comment Two to test on The published post"
  },
  comment: {}
}
const seedDatabase = async () => {

  // wip data before each test
  await prisma.mutation.deleteManyComments();
  await prisma.mutation.deleteManyPosts();
  await prisma.mutation.deleteManyUsers();

  // create Dummy user One for testing with same logic as in resolver
  userOne.user = await prisma.mutation.createUser({
    data: userOne.input
  });

  // create Dummy user One for testing with same logic as in resolver
  userTwo.user = await prisma.mutation.createUser({
    data: userTwo.input
  });

  // generate token
  userOne.jwt = generateToken(userOne.user.id);
  userTwo.jwt = generateToken(userTwo.user.id);

  // create 2 dummy posts
  postOne.post = await prisma.mutation.createPost({
    data: {
      ...postOne.input,
      author: {
        connect: {
          id: userOne.user.id
        }
      }
    }
  });


  postTwo.post = await prisma.mutation.createPost({
    data: {
      ...postTwo.input,
      author: {
        connect: {
          id: userOne.user.id
        }
      }
    }
  });

  // create dummy comments
  commentOne.comment = await prisma.mutation.createComment({
    data: {
      ...commentOne.input,
      author: {
        connect: {
          id: userTwo.user.id
        }
      },
      post: {
        connect: {
          id: postOne.post.id
        }
      }
    }
  });

  commentTwo.comment = await prisma.mutation.createComment({
    data: {
      ...commentTwo.input,
      author: {
        connect: {
          id: userOne.user.id
        }
      },
      post: {
        connect: {
          id: postOne.post.id
        }
      }
    }
  });
}

export { seedDatabase as default, userOne, userTwo, postOne, postTwo, commentOne, commentTwo }
