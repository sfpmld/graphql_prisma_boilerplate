import ApolloBoost, { gql } from 'apollo-boost';

const GRAPHQL_ENDPOINT = process.env.GRAPHQL_ENDPOINT;

const getClient = (jwt) => {

  // create apollo client
  return new ApolloBoost({
    uri: GRAPHQL_ENDPOINT,
    request: operation => {
      if(jwt){
        operation.setContext({
          headers: {
            Authorization: `Bearer ${jwt}`
          }
        });
      }
		},
  });
}

export { getClient as default }
