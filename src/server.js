// prerequisite
import { GraphQLServer, PubSub } from 'graphql-yoga';
import { resolvers, fragmentReplacements } from './resolvers/index';

//prisma
import prisma from './prisma';

//subscription
const pubsub = new PubSub();


//Server
const server = new GraphQLServer({
  typeDefs: "./src/schema.graphql",
  resolvers,
  context(req){
    return {
      pubsub,
      prisma,
      req
    }
  },
  fragmentReplacements
});

export { server as default }
