import getUserId from '../services/get-userid';


const Query = {
    users(parent, args, { prisma }, info) {
        const opArgs = {}

        if (args.query) {
            opArgs.where =
              {
                name_contains: args.query
              }
        }

        return prisma.query.users(opArgs, info)
    },
    async me(parent, args, { prisma, req }, info) {
      const userId = getUserId(req)
      const users = await prisma.query.users({
                  where: {
                      id: userId
                  }
              });

      return users[0];
    },
    posts(parent, args, { prisma }, info) {
        // default for public request
        const opArgs = {
          where:{
            published: true
          }
        }

        //if no publics queries
        if (args.query) {
            opArgs.where.OR = [{
                  title_contains: args.query
              }, {
                  body_contains: args.query
            }]
        }

        return prisma.query.posts(opArgs, info)
    },
    async myPosts(parent, args, { prisma, req }, info){
      const userId = getUserId(req);

      const opArgs = {}

      opArgs.where = {
        author: {
          id: userId
        }
      }

      if(args.query){
        opArgs.where.AND = [{
          title_contains:  args.query
        },{
          body_contains: args.query
        }]
      }

      return await prisma.query.posts(opArgs, info);

    },
    comments(parent, args, { prisma }, info) {
        return prisma.query.comments(null, info)
    },
    async post(parent, { id }, { prisma, req }, info){

      // authorize user and retrieving user ID
      const userId = getUserId(req, false);

      const posts = await prisma.query.posts({
        where: {
          id,
          OR:[
            { published: true },
            { author: {
                id: userId
            } }
          ]
        }
      },info);

      if(posts.length === 0){
        throw new Error('Post not found.');
      }

      return posts[0];

    },
}

export { Query as default }
