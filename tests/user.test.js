import '@babel/polyfill';
import 'cross-fetch/polyfill';

import seedDatabase, { userOne, userTwo } from "./utils/seedDatabase";
import {
  createUser,
  getUsers,
  login,
  getProfile,
} from "./utils/operations";
import getClient from './utils/getClient';

import { gql } from 'apollo-boost';
import prisma from '../src/prisma';


// create apollo client
const client = getClient();

// wipe our datas before each test suite
beforeEach(seedDatabase);




test('Should create a new user', async () => {

  const variables = {
    data: {
      name: "Xavier A.",
      email: "tester@test.com",
      password: "password"
    }
  }

  const response = await client.mutate({
    mutation: createUser,
    variables
  });

  // test if user has been created successfully
  const userCreated = await prisma.exists.User({
      id: response.data.createUser.user.id
  });


  expect(userCreated).toBe(true);


});

test('should expose public authors profiles', async () => {

  const response = await client.query({
    query: getUsers
  });

  expect(response.data.users.length).toBe(2);
  expect(response.data.users[0].email).toBe(null);
  expect(response.data.users[0].name).toBe('First Tester');

});


test('should not login with bad credentials', async () => {

  const variables = {
    data: {
      email: "tester@tes.com",
      password: "BADpassword"
    }
  }

  await expect(client.mutate({
    mutation: login,
    variables
  })).rejects.toThrow();
});

test('should fetch user profile', async () => {

  const client = getClient(userOne.jwt);

  const { data } = await client.query({
    query: getProfile
  });

  // test if user retrieved by me() is the same as the test user
  expect(data.me.id).toBe(userOne.user.id);
  expect(data.me.name).toEqual(userOne.user.name);
  expect(data.me.email).toEqual(userOne.user.email);

});

