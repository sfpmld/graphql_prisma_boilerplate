import bcrypt from 'bcrypt';

const hashPass = password => {

  // check if password length is enough
  if(password.length < 8){
    throw new Error('Password must be 8 characters or longer.');
  }

  return bcrypt.hash(password, 12);
};

const hashPassSync = password => {

  // check if password length is enough
  if(password.length < 8){
    throw new Error('Password must be 8 characters or longer.');
  }

  return bcrypt.hashSync(password, 12);
};

const checkPass = async ( password_1, password_2 ) => {

  return await bcrypt.compare(password_1, password_2);

}

export { hashPass, hashPassSync, checkPass }
