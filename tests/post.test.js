import '@babel/polyfill';
import 'cross-fetch/polyfill';
import { gql } from 'apollo-boost';

import seedDatabase, { userOne, userTwo, postOne, postTwo, commentOne, commentTwo } from "./utils/seedDatabase";
import {
  getPosts,
  getMyPost,
  updatePost,
  createPost,
  deletePost,
  subscribeToPosts
} from "./utils/operations";

import prisma from '../src/prisma';

import getClient from './utils/getClient';
import getSubClient  from './utils/getSubscClient';


// create apollo client
const client = getClient();

// wipe our datas before each test suite
beforeEach(seedDatabase);

test('should expose publics posts', async () => {

  const response = await client.query({
    query: getPosts
  });

  expect(response.data.posts.length).toBe(1);
  expect(response.data.posts[0].published).toBe(true);
});

test('should fetch user\'s list of posts', async () => {

  const client = getClient(userOne.jwt);


  const { data } = await client.query({
    query: getMyPost
  });

  expect(data.myPosts.length).toEqual(2);

});


test('should be able to update own posts', async () => {

  const client = getClient(userOne.jwt);

  const variables = {
    id: postOne.post.id,
    data: {
      published: false
    }
  }

  const { data } = await client.mutate({
    mutation: updatePost,
    variables
  });

  // assertion to test updated value
  expect(data.updatePost.published).toBe(false);
  //alternative
  const exists = await prisma.exists.Post({
      id: postOne.post.id,
      published: false
  });
  expect(exists).toBe(true);

});

test('should be able to create a Post', async () => {

  const client = getClient(userOne.jwt);

  const variables = {
    data:  {
      title: "create Post test",
      body: "content for testing create post",
      published: true
    }
  }

  const { data } = await client.mutate({
    mutation: createPost,
    variables
  })

  expect(data.createPost.title).toBe('create Post test');
  expect(data.createPost.body).toBe('content for testing create post');
  expect(data.createPost.published).toBe(true);

});

test('should be able to delete a post', async () => {

  const client = getClient(userOne.jwt);

  const variables = {
    id: postOne.post.id
  }

  const { data } = await client.mutate({
    mutation: deletePost,
    variables
  });

  const exists = await prisma.exists.Post({
      id: data.deletePost.id
  });

  expect(exists).toBe(false);

});

test('should subscribe to post', (done) => {

  const wsClient = getSubClient();

  wsClient.subscribe({
    query: subscribeToPosts
  }).subscribe({
    next(response){
      expect(response.data.post.mutation).toEqual('DELETED');
      done()
    }
  });

  prisma.mutation.deletePost({
    where:{
      id: postOne.post.id
    }
  }).then(res => {})

});
