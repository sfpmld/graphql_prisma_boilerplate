// import babel-polyfill for production enabling
import '@babel/polyfill/noConflict';

// env vars
const PORT = process.env.PORT || '8585';

// import graphQL Server
import server from './server';


//Server Options
const options = {
  port: PORT
}

server.start(
  options,
  ({ port }) => console.log("graphql is running on the port" + port)
);
