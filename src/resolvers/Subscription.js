import getUserId from '../services/get-userid';


const Subscription = {
    comment: {
        subscribe(parent, { postId }, { prisma, req }, info){
            return prisma.subscription.comment({
                where: {
                    node: {
                        post: {
                            id: postId
                        }
                    }
                }
            }, info)
        }
    },
    post: {
        subscribe(parent, args, { prisma, req }, info) {
            return prisma.subscription.post({
                where: {
                    node: {
                        published: true
                    }
                }
            }, info)
        }
    },
    myPosts: {
      subscribe(parent, args, { prisma, req }, info){
      const userId = getUserId(req);

        return prisma.subscription.post({
          where: {
            node: {
              author: {
                userId
              }
            }
          }
        }, info)
      },
    },
}

export { Subscription as default }
