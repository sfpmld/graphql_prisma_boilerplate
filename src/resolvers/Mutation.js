import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import getUserId from '../services/get-userid';
import { generateToken } from '../utils/jwt-utils';
import { hashPass, checkPass } from '../utils/bcrypt-utils';

//env vars
const JWT_TOKEN = process.env.JWT_TOKEN;

const Mutation = {
    async createUser(parent, { data }, { prisma }, info) {

        // validating password
        if(data.password.length < 8){
          throw new Error('Password must be 8 characters or longer.')
        }

        // hashing password send
        const password = await hashPass(data.password);

        const user = await prisma.mutation.createUser({
          data: {
            ...data,
            password
          }
        });

        // generating jwt token if authentified
        const token = generateToken(user.id);

        return {
          user,
          token
        }
    },
    async login(parent, { data }, { prisma }, info){
      // retrieving user by email
        const user = await prisma.query.user({
          where: {
            email: data.email
          }
        })

        if(!user){
          throw new Error('User not found.');
        }

        // comparing password with data in db
        const areSame = await bcrypt.compare(data.password, user.password);

        if(!areSame){
          throw new Error('Bad Password.');
        }

        // generating jwt token if authentified
        const token = generateToken(user.id);

        return {
          token,
          user
        }

    },
    async deleteUser(parent, args, { prisma, req }, info) {

        const userId = getUserId(req);

        return prisma.mutation.deleteUser({
            where: {
                id: userId
            }
        }, info)
    },
    async updateUser(parent, { data  }, { prisma, req }, info) {

        const userId = getUserId(req);

        // if password filled to be updated
        if(typeof data.password !== 'undefined'){
          data.password = await hashPass(data.password)
        }

        return prisma.mutation.updateUser({
          where: {
              id: userId
          },
          data
        }, info)
    },
    async createPost(parent, { data }, { prisma, req }, info) {

        const userId = getUserId(req);

        return prisma.mutation.createPost({
            data: {
                title: data.title,
                body: data.body,
                published: data.published,
                author: {
                    connect: {
                        id: userId
                    }
                }
            }
        }, info)
    },
    async deletePost(parent, args, { prisma, req }, info) {

        const userId = getUserId(req);

        // check if postExist
        const postExist = await prisma.exists.Post({
          id: args.id,
          author: {
            id: userId
          }
        });

        if(!postExist){
          throw new Error('Post not found');
        }

        return prisma.mutation.deletePost({
            where: {
                id: args.id
            }
        }, info)
    },
    async updatePost(parent, { id, data }, { prisma, req }, info) {

        // retrieving user ID
        const userID = getUserId(req);

        // check if this post exists
        const postExist = await prisma.exists.Post({
          author: {
            id: userID
          },
          id: id
        });

        if(!postExist){
          throw new Error('Post not found.');
        }

        // check if the post is published
        const postPublished = await prisma.exists.Post({
            published: true,
            id
        });

        //if post published and about to be unpublished -> remove all comment
        //associated with this post
        if(postPublished && data.published === false){
          await prisma.mutation.deleteManyComments({
            where: {
              post: {
                id: data.id
              }
            }
          });
        }

        //updating post
        return prisma.mutation.updatePost({
            where: {
                id
            },
            data
        }, info);
    },
    async createComment(parent, { data }, { prisma, req }, info) {

        // retrieving userId by validating token
        const userId = getUserId(req);

        const isPublished = await prisma.exists.Post({
            id: args.data.post,
            published: true
        });

        if(!isPublished){
          throw new Error('Unable to find post.');
        }

        return prisma.mutation.createComment({
            data: {
                text: data.text,
                author: {
                    connect: {
                        id: userId
                    }
                },
                post: {
                    connect: {
                        id: data.post
                    }
                }
            }
        }, info)
    },
    async deleteComment(parent, { id }, { prisma, req }, info) {

        // retrieving userId by validating token
        const userId = getUserId(req);

        // check if user is authorized to delete this comment
        const ownComment = await prisma.exists.Comment({
          id,
          author: {
            id: userId
          }
        });

        if(!ownComment){
          throw new Error('Not authorized to delete this comment.')
        }

        return prisma.mutation.deleteComment({
            where: { id }
        }, info);
    },
    async updateComment(parent, { id, data }, { prisma, req }, info) {
        // retrieving userId by validating token
        const userId = getUserId(req);

        // check if comment exists
        const commentExists = await prisma.exists.Comment({ id });

        if(!commentExists){
          throw new Error('Comment not exists.');
        }
        return prisma.mutation.updateComment({
            where: { id }
        }, info);
    }
}

export { Mutation as default }
